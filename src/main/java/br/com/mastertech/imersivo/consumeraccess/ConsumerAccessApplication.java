package br.com.mastertech.imersivo.consumeraccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerAccessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerAccessApplication.class, args);
	}

}
