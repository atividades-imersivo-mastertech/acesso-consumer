package br.com.mastertech.imersivo.consumeraccess.controller;

import br.com.mastertech.imersivo.consumeraccess.service.AccessConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import br.com.mastertech.imersivo.produceraccess.model.Access;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

@Component
public class AccessConsumerController {
    @Autowired
    private AccessConsumerService accessConsumerService;

    @KafkaListener(topics = "kaique", groupId = "kaiquera")
    public void consumeAccess(@Payload Access access) {
        accessConsumerService.writeAcessLogFiles("/Users/mobile/Desktop/allowAcess.csv", "/Users/mobile/Desktop/denyAcess.csv", access);
    }
}

