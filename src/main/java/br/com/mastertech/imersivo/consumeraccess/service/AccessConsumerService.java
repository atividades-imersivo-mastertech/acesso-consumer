package br.com.mastertech.imersivo.consumeraccess.service;

import br.com.mastertech.imersivo.produceraccess.model.Access;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

@Service
public class AccessConsumerService {

    public void writeAcessLogFiles(String allowedFile, String notAllowedFile, Access access) {
        try {
            String registro = String.format("%s,%s,%s\n", access.getClientId(), access.getDoorId(), LocalDateTime.now());
            if (access.getAccessAllowed()) {
                Files.write(Paths.get(allowedFile), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } else {
                Files.write(Paths.get(notAllowedFile), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
